package auth

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"
)

// Errors.
var (
	ErrAuthFailed = errors.New("Wrong Credential")
)

var (
	clientOnce sync.Once
	auth       Auth
)

// Auth interface provides authentication methods.
type Auth interface {
	IssueToken(clientID, clientSecret string) error
	ClearAuth()
	AccessToken() (string, error)
	HasValidCredential() bool
	CurrentToken() *Auth0Token
}

// Auth0Token describes JWT response from Auth0.
type Auth0Token struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
	TokenType   string `json:"token_type"`
	ExpiresAt   int64  `json:"expires_at"`
}

type authClient struct {
	mu    sync.Mutex
	Token *Auth0Token
}

func (c *authClient) IssueToken(clientID, clientSecret string) error {
	var (
		credentials []byte
		err         error
		req         *http.Request
	)

	if credentials, err = json.Marshal(&map[string]string{
		"grant_type":    "client_credentials",
		"audience":      "https://api.ino-vibe.ino-on.net",
		"client_id":     clientID,
		"client_secret": clientSecret,
	}); err != nil {
		panic(err)
	}

	if req, err = http.NewRequest(
		"POST",
		"https://ino-vibe.auth0.com/oauth/token",
		strings.NewReader(string(credentials))); err != nil {
		panic(err)
	}
	req.Header.Add("Content-Type", "application/json")

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusUnauthorized {
		return ErrAuthFailed
	}

	respData, err := ioutil.ReadAll(resp.Body)

	var auth0Token Auth0Token
	if err = json.Unmarshal(respData, &auth0Token); err != nil {
		panic(err)
	}

	const ExpMarginSec = 600
	auth0Token.ExpiresAt = (time.Now().Unix() + auth0Token.ExpiresIn - ExpMarginSec)

	c.mu.Lock()
	defer c.mu.Unlock()

	c.Token = &auth0Token

	return nil
}

func (c *authClient) ClearAuth() {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.Token = nil
}

func (c *authClient) AccessToken() (string, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.Token == nil {
		return "", ErrAuthFailed
	}

	return c.Token.AccessToken, nil
}

func (c *authClient) HasValidCredential() bool {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.Token == nil {
		return false
	}

	curTimestamp := time.Now().Unix()

	const ExpireMarginSec = int64(60 * 60)
	if curTimestamp+ExpireMarginSec >= c.Token.ExpiresAt {
		return false
	}
	return true
}

func (c *authClient) CurrentToken() *Auth0Token {
	return c.Token
}

/*
NewClient creates new client instance for Auth0 authentication.
*/
func NewClient() Auth {
	clientOnce.Do(func() {
		auth = &authClient{}
	})
	return auth
}
