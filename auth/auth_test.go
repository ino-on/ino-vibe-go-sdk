package auth

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type AuthTestSuite struct {
	suite.Suite

	AuthClient Auth
}

func (s *AuthTestSuite) SetupTest() {
	s.AuthClient = NewClient()
}

func (s *AuthTestSuite) AfterTest(suiteName, testName string) {
	s.AuthClient.ClearAuth()
}

func (s *AuthTestSuite) TestAuth() {
	clientID := "JGIf60FQOwD17ByaWm4fwfGZELhZVXPW"
	clientSecret := "VBzKiyQ-2ADnpRclVasO5aDyUe1JquekRp7QzyzcEt85_NwBlqSHFX7WAu2MsHx0"

	err := s.AuthClient.IssueToken(clientID, clientSecret)
	accessToken, err := s.AuthClient.AccessToken()

	assert.Nil(s.T(), err)
	assert.NotNil(s.T(), accessToken)
	assert.True(s.T(), s.AuthClient.HasValidCredential())
}

func (s *AuthTestSuite) TestAuthFailed() {
	err := s.AuthClient.IssueToken("", "")
	assert.Equal(s.T(), ErrAuthFailed, err)

	token, err := s.AuthClient.AccessToken()

	assert.Equal(s.T(), "", token)
	assert.Equal(s.T(), ErrAuthFailed, err)
	assert.False(s.T(), s.AuthClient.HasValidCredential())
}

func (s *AuthTestSuite) TestAuthExpires() {
	current := time.Now()

	dummyToken := Auth0Token{}
	dummyToken.ExpiresAt = current.Unix() - (2 * 60 * 60)

	dummyClient := authClient{Token: &dummyToken}

	assert.False(s.T(), dummyClient.HasValidCredential())
}

func TestAuth(t *testing.T) {
	suite.Run(t, new(AuthTestSuite))
}
