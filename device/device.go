package device

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"bitbucket.org/ino-on/ino-vibe-go-sdk/auth"
)

type deviceResponse struct {
	Devices []Device `json:"devices"`
}

// Type describes InoVibe device types.
type Type string

// Device Types
const (
	InoVibe    Type = "InoVibe"
	InoVibeS   Type = "InoVibeS"
	InoVibePro Type = "InoVibePro"
)

// InstallType is current install status of device.
type InstallType string

// Install types.
const (
	Initial   InstallType = "Initial"
	Requested InstallType = "Requested"
	Installed InstallType = "Installed"
	Discarded InstallType = "Discarded"
)

// SensorRangeType is accelerometer's sensing range.
type SensorRangeType string

// SensorRange types.
const (
	Gravity2G  SensorRangeType = "Gravity2G"
	Gravity4G  SensorRangeType = "Gravity4G"
	Gravity8G  SensorRangeType = "Gravity8G"
	Gravity16G SensorRangeType = "Gravity16G"
)

// Errors
var (
	ErrNonExistDevice = errors.New("Device is not found on system")
	ErrUnexpected     = errors.New("Unexpected Internal server error")
)

// Device describes device info.
type Device struct {
	DevID     string  `json:"devid"`
	DevType   Type    `json:"dev_type"`
	Alias     string  `json:"alias"`
	GroupID   string  `json:"group_id"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Installer string  `json:"installer"`

	InstallStatus InstallType `json:"install_status"`
	InstallDate   *time.Time  `json:"install_date"`
	UpdateDate    *time.Time  `json:"update_date"`
	AlivePeriod   int         `json:"period"`
	Battery       int         `json:"battery"`
	Temperature   int         `json:"temperature"`
	RSSI          int         `json:"rssi"`
	IsAlarmed     bool        `json:"is_alarmed"`
	AlarmDate     *time.Time  `json:"alarm_date"`
	IsDeviceOK    bool        `json:"is_device_ok"`

	SensorRange    SensorRangeType `json:"sensor_range"`
	SampleRate     int             `json:"sample_rate"`
	WaveBlocks     int             `json:"wave_blocks"`
	IsEnableNotify bool            `json:"is_notif_enabled"`

	AppFWVer  string `json:"app_fw_ver"`
	LoRaFWVer string `json:"lora_fw_ver"`

	RecogParam0 float64 `json:"recog_param_0"`
	RecogParam1 float64 `json:"recog_param_1"`
	RecogParam2 float64 `json:"recog_param_2"`
}

// Client provides Device related API interfaces.
type Client interface {
	GetDevice(token, devid string) (*Device, error)
	ListDevice(token string) ([]Device, error)
	DoUpdateStatus(token, devid string, request StatusUpdateRequest) error
	DoUpdateConfig(token, devid string, request ConfigUpdateRequest) error
}

type client struct{}

func (c *client) GetDevice(token, devid string) (*Device, error) {
	req, err := http.NewRequest("GET", "https://api.ino-vibe.ino-on.net/v3/device/"+devid, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+token)

	httpClient := http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {
		respData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		var response deviceResponse

		err = json.Unmarshal(respData, &response)
		if err != nil {
			return nil, err
		}

		if len(response.Devices) == 0 {
			return nil, ErrNonExistDevice
		}

		//
		// gRPC server returns 0 for Initial status, so real response value is omited.
		//
		if response.Devices[0].InstallStatus == "" {
			response.Devices[0].InstallStatus = Initial
		}

		return &response.Devices[0], nil

	} else if resp.StatusCode == http.StatusNotFound {
		return nil, ErrNonExistDevice
	} else if resp.StatusCode == http.StatusUnauthorized {
		return nil, auth.ErrAuthFailed
	} else {
		fmt.Println(resp.StatusCode)
		return nil, nil
	}
}

func (c *client) ListDevice(token string) ([]Device, error) {
	req, err := http.NewRequest("GET", "https://api.ino-vibe.ino-on.net/v3/device", nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+token)

	httpClient := http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {
		respData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return []Device{}, err
		}

		var response deviceResponse

		err = json.Unmarshal(respData, &response)
		if err != nil {
			return []Device{}, err
		}

		for i := 0; i < len(response.Devices); i++ {
			if response.Devices[i].InstallStatus == "" {
				response.Devices[i].InstallStatus = Initial
			}
		}

		return response.Devices, nil
	} else if resp.StatusCode == http.StatusUnauthorized {
		return []Device{}, auth.ErrAuthFailed
	} else {
		fmt.Println(resp.StatusCode)
		return []Device{}, ErrUnexpected
	}

}

func (c *client) DoUpdateStatus(token, devid string, request StatusUpdateRequest) error {
	// TODO: Temporary added this code because update request on non exist device invoke system
	// error. gRPC server should be fixed.
	_, err := c.GetDevice(token, devid)
	if err == ErrNonExistDevice {
		return err
	}

	reqBody := map[string]interface{}{}

	if installStatus, err := request.GetInstallStatus(); err == nil {
		reqBody["install_status_value"] = installStatus
	}

	if updateDate, err := request.GetUpdateDate(); err == nil {
		reqBody["update_date_value"] = updateDate.Format(time.RFC3339)
	}

	if isAlarmed, err := request.GetIsAlarmed(); err == nil {
		reqBody["is_alarmed_value"] = isAlarmed
	}

	if alarmDate, err := request.GetAlarmDate(); err == nil {
		reqBody["alarm_date_value"] = alarmDate.Format(time.RFC3339)
	}

	if isDeviceOK, err := request.GetIsDeviceOK(); err == nil {
		reqBody["is_device_ok_value"] = isDeviceOK
	}

	if battery, err := request.GetBattery(); err == nil {
		reqBody["battery_value"] = battery
	}

	if temperature, err := request.GetTemperature(); err == nil {
		reqBody["temperature_value"] = temperature
	}

	if rssi, err := request.GetRSSI(); err == nil {
		reqBody["rssi_value"] = rssi
	}

	bodyData, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	url := fmt.Sprintf("https://api.ino-vibe.ino-on.net/v3/device/%s/status", devid)
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(bodyData))
	if err != nil {
		return err
	}

	req.Header.Add("Authorization", "Bearer "+token)
	req.Header.Add("Content-Type", "application/json;charset=utf-8")

	httpClient := http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusInternalServerError {
		return ErrUnexpected
	}

	return nil
}

func (c *client) DoUpdateConfig(token, devid string, request ConfigUpdateRequest) error {
	_, err := c.GetDevice(token, devid)
	if err == ErrNonExistDevice {
		return err
	}

	reqBody := map[string]interface{}{}

	if recogParam0, err := request.GetRecogParam0(); err == nil {
		reqBody["recog_param_0_value"] = recogParam0
	}

	if recogParam1, err := request.GetRecogParam1(); err == nil {
		reqBody["recog_param_1_value"] = recogParam1
	}

	if recogParam2, err := request.GetRecogParam2(); err == nil {
		reqBody["recog_param_2_value"] = recogParam2
	}

	if notifEnabled, err := request.GetNotifEnabled(); err == nil {
		reqBody["is_notif_enabled_value"] = notifEnabled
	}

	if waveBlocks, err := request.GetWaveBlocks(); err == nil {
		reqBody["wave_blocks_value"] = waveBlocks
	}

	bodyData, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	url := fmt.Sprintf("https://api.ino-vibe.ino-on.net/v3/device/%s/config", devid)
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(bodyData))
	if err != nil {
		return err
	}

	req.Header.Add("Authorization", "Bearer "+token)
	req.Header.Add("Content-Type", "application/json;charset=utf-8")

	httpClient := http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusInternalServerError {
		return ErrUnexpected
	}

	return nil
}

// NewClient function create new Device API client.
func NewClient() Client {
	return &client{}
}
