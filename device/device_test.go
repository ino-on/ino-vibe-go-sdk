package device

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"bitbucket.org/ino-on/ino-vibe-go-sdk/auth"
)

type DeviceTestSuite struct {
	suite.Suite

	Token        *auth.Auth0Token
	DeviceClient Client
}

func (s *DeviceTestSuite) SetupSuite() {
	tokenData, err := ioutil.ReadFile("../token.json")
	s.Token = &auth.Auth0Token{}

	err = json.Unmarshal(tokenData, s.Token)
	if err != nil {
		panic(err)
	}

	s.DeviceClient = NewClient()
}

func (s *DeviceTestSuite) TestGetDevice() {
	const devid = "000000030000000000000001"

	accessToken := s.Token.AccessToken
	device, err := s.DeviceClient.GetDevice(accessToken, devid)

	assert.Nil(s.T(), err)
	assert.NotNil(s.T(), device)
	assert.Equal(s.T(), devid, device.DevID)
}

func (s *DeviceTestSuite) TestGetDeviceNonExist() {
	accessToken := s.Token.AccessToken
	device, err := s.DeviceClient.GetDevice(accessToken, "non-exist-device-id-here")

	assert.Nil(s.T(), device)
	assert.Equal(s.T(), ErrNonExistDevice, err)
}

func (s *DeviceTestSuite) TestGetDeviceInvalidAuth() {
	device, err := s.DeviceClient.GetDevice("Invalid token", "non-exist-device-id-here")

	assert.Nil(s.T(), device)
	assert.Equal(s.T(), auth.ErrAuthFailed, err)
}

func (s *DeviceTestSuite) TestListDevice() {
	devices, err := s.DeviceClient.ListDevice(s.Token.AccessToken)

	assert.Nil(s.T(), err)
	assert.True(s.T(), len(devices) > 0)
}

func (s *DeviceTestSuite) TestUpdateDevice() {
	const devid = "000000030000000000000001"
	accessToken := s.Token.AccessToken

	expectations := []struct {
		InstallStatus InstallType
		UpdateDate    time.Time
		IsAlarmed     bool
		IsDeviceOK    bool
		AlarmDate     time.Time
		Battery       int
		Temperature   int
		RSSI          int
	}{
		{
			InstallStatus: Initial,
			UpdateDate:    time.Now(),
			IsAlarmed:     true,
			IsDeviceOK:    false,
			AlarmDate:     time.Now(),
			Battery:       100,
			Temperature:   30,
			RSSI:          -80,
		},
		{
			InstallStatus: Installed,
			UpdateDate:    time.Now(),
			IsAlarmed:     false,
			IsDeviceOK:    true,
			AlarmDate:     time.Now(),
			Battery:       50,
			Temperature:   10,
			RSSI:          -100,
		},
	}

	for _, expect := range expectations {
		req := NewStatusUpdateRequest()
		req.SetInstallStatus(expect.InstallStatus).
			SetUpdateDate(expect.UpdateDate).
			SetIsAlarmed(expect.IsAlarmed).
			SetAlarmDate(expect.AlarmDate).
			SetIsDeviceOK(expect.IsDeviceOK).
			SetBattery(expect.Battery).
			SetTemperature(expect.Temperature).
			SetRSSI(expect.RSSI)

		err := s.DeviceClient.DoUpdateStatus(accessToken, devid, req)

		fmt.Printf("%+v\n", req)
		fmt.Println(err)

		device, _ := s.DeviceClient.GetDevice(accessToken, devid)
		fmt.Printf("%+v\n", device)

		assert.Equal(s.T(), expect.InstallStatus, device.InstallStatus)
		assert.Equal(s.T(), expect.UpdateDate.Unix(), device.UpdateDate.Unix())
		assert.Equal(s.T(), expect.IsAlarmed, device.IsAlarmed)
		assert.Equal(s.T(), expect.AlarmDate.Unix(), device.AlarmDate.Unix())
		assert.Equal(s.T(), expect.IsDeviceOK, device.IsDeviceOK)
		assert.Equal(s.T(), expect.Battery, device.Battery)
		assert.Equal(s.T(), expect.Temperature, device.Temperature)
		assert.Equal(s.T(), expect.RSSI, device.RSSI)
	}

}

func (s *DeviceTestSuite) TestConfigDevice() {
	const devid = "000000030000000000000001"
	accessToken := s.Token.AccessToken

	expectations := []struct {
		RecogParam0  float64
		RecogParam1  float64
		RecogParam2  float64
		NotifEnabled bool
		WaveBlocks   int
	}{
		{
			RecogParam0:  12,
			RecogParam1:  0.6,
			RecogParam2:  8.0,
			NotifEnabled: true,
			WaveBlocks:   12,
		},
		{
			RecogParam0:  10,
			RecogParam1:  0.3,
			RecogParam2:  6.0,
			NotifEnabled: false,
			WaveBlocks:   2,
		},
	}

	for _, expect := range expectations {

		req := NewConfigUpdateRequest()
		req.SetRecogParam0(expect.RecogParam0).
			SetRecogParam1(expect.RecogParam1).
			SetRecogParam2(expect.RecogParam2).
			SetNotifEnabled(expect.NotifEnabled).
			SetWaveBlocks(expect.WaveBlocks)

		err := s.DeviceClient.DoUpdateConfig(accessToken, devid, req)
		assert.Nil(s.T(), err)

		device, _ := s.DeviceClient.GetDevice(accessToken, devid)

		assert.Equal(s.T(), expect.RecogParam0, device.RecogParam0)
		assert.Equal(s.T(), expect.RecogParam1, device.RecogParam1)
		assert.Equal(s.T(), expect.RecogParam2, device.RecogParam2)
		assert.Equal(s.T(), expect.NotifEnabled, device.IsEnableNotify)
		assert.Equal(s.T(), expect.WaveBlocks, device.WaveBlocks)
	}

}

// Non exist device.
func (s *DeviceTestSuite) TestUpdateDeviceNonExist() {
	req := NewStatusUpdateRequest()
	err := s.DeviceClient.DoUpdateStatus(s.Token.AccessToken, "non-exist-device", req)

	assert.Equal(s.T(), ErrNonExistDevice, err)
}

func TestDevice(t *testing.T) {
	suite.Run(t, new(DeviceTestSuite))
}
