package device

import (
	"errors"
	"time"
)

// Errors
var (
	ErrEmptyValue = errors.New("Value not set")
)

// StatusUpdateRequest represents user's request to update device's status.
type StatusUpdateRequest interface {
	SetInstallStatus(installStatus InstallType) StatusUpdateRequest
	GetInstallStatus() (InstallType, error)

	SetUpdateDate(date time.Time) StatusUpdateRequest
	GetUpdateDate() (time.Time, error)

	SetIsAlarmed(isAlarmed bool) StatusUpdateRequest
	GetIsAlarmed() (bool, error)

	SetAlarmDate(date time.Time) StatusUpdateRequest
	GetAlarmDate() (time.Time, error)

	SetIsDeviceOK(isDeviceOK bool) StatusUpdateRequest
	GetIsDeviceOK() (bool, error)

	SetBattery(battery int) StatusUpdateRequest
	GetBattery() (int, error)

	SetTemperature(temperature int) StatusUpdateRequest
	GetTemperature() (int, error)

	SetRSSI(rssi int) StatusUpdateRequest
	GetRSSI() (int, error)
}

type statusUpdateRequest struct {
	installStatus        InstallType
	installStatusChanged bool
	updateDate           time.Time
	updateDateChanged    bool
	isAlarmed            bool
	isAlarmedChanged     bool
	alarmDate            time.Time
	alarmDateChanged     bool
	isDeviceOK           bool
	isDeviceOKChanged    bool
	battery              int
	batteryChanged       bool
	temperature          int
	temperatureChanged   bool
	rssi                 int
	rssiChanged          bool
}

func (r *statusUpdateRequest) SetInstallStatus(installStatus InstallType) StatusUpdateRequest {
	r.installStatus = installStatus
	r.installStatusChanged = true
	return r
}

func (r *statusUpdateRequest) GetInstallStatus() (InstallType, error) {
	if r.installStatusChanged {
		return r.installStatus, nil
	}

	return Initial, ErrEmptyValue
}

func (r *statusUpdateRequest) SetUpdateDate(date time.Time) StatusUpdateRequest {
	r.updateDate = date
	r.updateDateChanged = true
	return r
}

func (r *statusUpdateRequest) GetUpdateDate() (time.Time, error) {
	if r.updateDateChanged {
		return r.updateDate, nil
	}

	return time.Unix(0, 0), ErrEmptyValue
}

func (r *statusUpdateRequest) SetIsAlarmed(isAlarmed bool) StatusUpdateRequest {
	r.isAlarmed = isAlarmed
	r.isAlarmedChanged = true
	return r
}

func (r *statusUpdateRequest) GetIsAlarmed() (bool, error) {
	if r.isAlarmedChanged {
		return r.isAlarmed, nil
	}

	return false, ErrEmptyValue
}

func (r *statusUpdateRequest) SetAlarmDate(date time.Time) StatusUpdateRequest {
	r.alarmDate = date
	r.alarmDateChanged = true
	return r
}

func (r *statusUpdateRequest) GetAlarmDate() (time.Time, error) {
	if r.alarmDateChanged {
		return r.alarmDate, nil
	}

	return time.Unix(0, 0), ErrEmptyValue
}

func (r *statusUpdateRequest) SetIsDeviceOK(isDeviceOK bool) StatusUpdateRequest {
	r.isDeviceOK = isDeviceOK
	r.isDeviceOKChanged = true
	return r
}
func (r *statusUpdateRequest) GetIsDeviceOK() (bool, error) {
	if r.isDeviceOKChanged {
		return r.isDeviceOK, nil
	}

	return false, ErrEmptyValue
}

func (r *statusUpdateRequest) SetBattery(battery int) StatusUpdateRequest {
	r.battery = battery
	r.batteryChanged = true
	return r
}

func (r *statusUpdateRequest) GetBattery() (int, error) {
	if r.batteryChanged {
		return r.battery, nil
	}

	return 0, ErrEmptyValue
}

func (r *statusUpdateRequest) SetTemperature(temperature int) StatusUpdateRequest {
	r.temperature = temperature
	r.temperatureChanged = true
	return r
}

func (r *statusUpdateRequest) GetTemperature() (int, error) {
	if r.temperatureChanged {
		return r.temperature, nil
	}

	return 0, ErrEmptyValue
}

func (r *statusUpdateRequest) SetRSSI(rssi int) StatusUpdateRequest {
	r.rssi = rssi
	r.rssiChanged = true
	return r
}
func (r *statusUpdateRequest) GetRSSI() (int, error) {
	if r.rssiChanged {
		return r.rssi, nil
	}

	return 0, ErrEmptyValue
}

// NewStatusUpdateRequest creates new request for update device's status.
func NewStatusUpdateRequest() StatusUpdateRequest {
	return &statusUpdateRequest{}
}

// ConfigUpdateRequest represents user's request to update device's config.
type ConfigUpdateRequest interface {
	SetNotifEnabled(value bool) ConfigUpdateRequest
	GetNotifEnabled() (bool, error)

	SetWaveBlocks(value int) ConfigUpdateRequest
	GetWaveBlocks() (int, error)

	SetRecogParam0(value float64) ConfigUpdateRequest
	GetRecogParam0() (float64, error)

	SetRecogParam1(value float64) ConfigUpdateRequest
	GetRecogParam1() (float64, error)

	SetRecogParam2(value float64) ConfigUpdateRequest
	GetRecogParam2() (float64, error)
}

type configUpdateRequest struct {
	notifEnabled        bool
	notifEnabledChanged bool

	WaveBlocks        int
	WaveBlocksChanged bool

	recogParam0        float64
	recogParam0Changed bool

	recogParam1        float64
	recogParam1Changed bool

	recogParam2        float64
	recogParam2Changed bool
}

func (r *configUpdateRequest) SetRecogParam0(value float64) ConfigUpdateRequest {
	r.recogParam0 = value
	r.recogParam0Changed = true
	return r
}

func (r *configUpdateRequest) GetRecogParam0() (float64, error) {
	if r.recogParam0Changed {
		return r.recogParam0, nil
	}
	return 0, ErrEmptyValue
}

func (r *configUpdateRequest) SetRecogParam1(value float64) ConfigUpdateRequest {
	r.recogParam1 = value
	r.recogParam1Changed = true
	return r
}

func (r *configUpdateRequest) GetRecogParam1() (float64, error) {
	if r.recogParam1Changed {
		return r.recogParam1, nil
	}
	return 0, ErrEmptyValue
}

func (r *configUpdateRequest) SetRecogParam2(value float64) ConfigUpdateRequest {
	r.recogParam2 = value
	r.recogParam2Changed = true
	return r
}

func (r *configUpdateRequest) GetRecogParam2() (float64, error) {
	if r.recogParam2Changed {
		return r.recogParam2, nil
	}
	return 0, ErrEmptyValue
}

func (r *configUpdateRequest) SetNotifEnabled(value bool) ConfigUpdateRequest {
	r.notifEnabled = value
	r.notifEnabledChanged = true
	return r
}

func (r *configUpdateRequest) GetNotifEnabled() (bool, error) {
	if r.notifEnabledChanged {
		return r.notifEnabled, nil
	}
	return false, ErrEmptyValue
}

func (r *configUpdateRequest) SetWaveBlocks(value int) ConfigUpdateRequest {
	r.WaveBlocks = value
	r.WaveBlocksChanged = true
	return r
}

func (r *configUpdateRequest) GetWaveBlocks() (int, error) {
	if r.WaveBlocksChanged {
		return r.WaveBlocks, nil
	}
	return 0, ErrEmptyValue
}

// NewConfigUpdateRequest creates new request for update devices' config.
func NewConfigUpdateRequest() ConfigUpdateRequest {
	return &configUpdateRequest{}
}
