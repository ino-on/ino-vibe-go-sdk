package group

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/ino-on/ino-vibe-go-sdk/auth"
)

// Client describes Group API interfaces.
type Client interface {
	GetByID(token, groupID string) (*Group, error)
	GetParentUsers(token, groupID string) ([]string, error)
	GetNestedUsers(token, groupID string) ([]string, error)
}

type groupClient struct{}

type groupResponse struct {
	Groups []Group `json:"groups"`
}

// Group is response type of Group API.
type Group struct {
	ID   string `json:"groupid"`
	Name string `json:"name"`
}

type nestedUsersResponse struct {
	Emails []string `json:"emails"`
}

// Errors
var (
	ErrNonExistGroup = errors.New("Selected group is not found on system")
	ErrUnexpected    = errors.New("ErrUnexpected")
)

func (c *groupClient) GetByID(token, groupID string) (*Group, error) {
	req, err := http.NewRequest("GET", "https://api.ino-vibe.ino-on.net/v3/group/"+groupID, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+token)

	httpClient := http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {
		respData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		groupResp := groupResponse{}
		err = json.Unmarshal(respData, &groupResp)
		if err != nil {
			return nil, err
		}

		fmt.Printf("%+v\n", groupResp)

		if len(groupResp.Groups) == 0 {
			return nil, ErrNonExistGroup
		}

		return &groupResp.Groups[0], nil
	} else if resp.StatusCode == http.StatusUnauthorized {
		return nil, auth.ErrAuthFailed
	} else {
		return nil, ErrUnexpected
	}
}

func (c *groupClient) GetParentUsers(token, groupID string) ([]string, error) {
	url := fmt.Sprintf("https://api.ino-vibe.ino-on.net/v3/group/%s/members/parent", groupID)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+token)

	httpClient := http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {
		respData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		resp := nestedUsersResponse{}
		err = json.Unmarshal(respData, &resp)
		if err != nil {
			return nil, err
		}

		return resp.Emails, nil
	} else if resp.StatusCode == http.StatusUnauthorized {
		return nil, auth.ErrAuthFailed
	} else {
		fmt.Println("StatusCode ", resp.StatusCode)
		return nil, ErrUnexpected
	}

	return []string{}, nil
}

func (c *groupClient) GetNestedUsers(token, groupID string) ([]string, error) {
	url := fmt.Sprintf("https://api.ino-vibe.ino-on.net/v3/group/%s/members/nested", groupID)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+token)

	httpClient := http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {
		respData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		resp := nestedUsersResponse{}
		err = json.Unmarshal(respData, &resp)
		if err != nil {
			return nil, err
		}

		return resp.Emails, nil
	} else if resp.StatusCode == http.StatusUnauthorized {
		return nil, auth.ErrAuthFailed
	} else {
		fmt.Println("StatusCode ", resp.StatusCode)
		return nil, ErrUnexpected
	}
}

// NewClient creates new client for Group API.
func NewClient() Client {
	return &groupClient{}
}
