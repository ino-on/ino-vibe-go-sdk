package group

import (
	"encoding/json"
	"io/ioutil"
	"testing"

	"bitbucket.org/ino-on/ino-vibe-go-sdk/auth"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type GroupTestSuite struct {
	suite.Suite

	Token       *auth.Auth0Token
	groupClient Client
}

func (s *GroupTestSuite) SetupTest() {
	tokenData, err := ioutil.ReadFile("../token.json")
	s.Token = &auth.Auth0Token{}

	err = json.Unmarshal(tokenData, s.Token)
	if err != nil {
		panic(err)
	}

	s.groupClient = NewClient()
}

func (s *GroupTestSuite) TestGroupSuccess() {
	const testGroupID = "0bee7b43-0b57-4b54-9062-430e2bd3fa79"

	group, err := s.groupClient.GetByID(s.Token.AccessToken, testGroupID)

	assert.Nil(s.T(), err)
	assert.Equal(s.T(), testGroupID, group.ID)
	assert.Equal(s.T(), "이노온", group.Name)
}

func (s *GroupTestSuite) TestGroupNonExist() {
	group, err := s.groupClient.GetByID(s.Token.AccessToken, "non-exist-group-id")

	assert.Equal(s.T(), ErrNonExistGroup, err)
	assert.Nil(s.T(), group)
}

func (s *GroupTestSuite) TestGroupWrongToken() {
	group, err := s.groupClient.GetByID("invalid-token", "non-exist-group-id")

	assert.Nil(s.T(), group)
	assert.Equal(s.T(), auth.ErrAuthFailed, err)
}

func (s *GroupTestSuite) TestNestedUserSuccess() {
	const testGroupID = "0bee7b43-0b57-4b54-9062-430e2bd3fa79"

	group, err := s.groupClient.GetNestedUsers(s.Token.AccessToken, testGroupID)

	assert.Nil(s.T(), err)
	assert.True(s.T(), len(group) > 0)
}

/* TODO:
func (s *GroupTestSuite) TestNestedUserNonExistGroup() {
	token, err := s.authClient.AccessToken()

	group, err := s.groupClient.GetNestedUsers(token, "non-exist-group")

	assert.Nil(s.T(), err)
	assert.True(s.T(), len(group) > 0)
}
*/

func (s *GroupTestSuite) TestParentUserSuccess() {
	testFixtures := []struct {
		GroupID    string
		Contain    string
		NotContain string
	}{
		{
			GroupID:    "0bee7b43-0b57-4b54-9062-430e2bd3fa79",
			Contain:    "parent_tester@ino-on.com",
			NotContain: "child_tester@ino-on.com,",
		},
		{
			GroupID:    "74fc3dda-c4d4-4589-bd9c-858c3d178d83",
			Contain:    "child_tester@ino-on.com",
			NotContain: "contact@ino-on.com,",
		},
	}

	for _, fixture := range testFixtures {
		users, err := s.groupClient.GetParentUsers(s.Token.AccessToken, fixture.GroupID)

		assert.Nil(s.T(), err)
		assert.Contains(s.T(), users, fixture.Contain)
		assert.NotContains(s.T(), users, fixture.NotContain)
	}
}

func TestGroup(t *testing.T) {
	suite.Run(t, new(GroupTestSuite))
}
